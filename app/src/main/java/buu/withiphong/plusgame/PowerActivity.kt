package buu.withiphong.plusgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_sum.*

class PowerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_power)
        var result = randomNum()
        val btnAnswer1 = findViewById<Button>(R.id.btnAnswer1)
        correctTextView.text = "Correct: "+countCorrect.toString()
        incorrectTextView.text ="Inorrect: "+countIncorrect.toString()
        btnAnswer1.setOnClickListener {
            val btnAnsId = 1
            val count = check(btnAnsId, result)
            if (count){
                countCorrect++
                correctTextView.text = "Correct: "+countCorrect.toString()
            }else{
                countIncorrect++
                incorrectTextView.text ="Inorrect: "+countIncorrect.toString()
            }
            //result = randomNum()
            backToActivity()
        }
        val btnAnswer2 = findViewById<Button>(R.id.btnAnswer2)
        btnAnswer2.setOnClickListener {
            val btnAnsId = 2
            val count = check(btnAnsId, result)
            if (count){
                countCorrect++
                correctTextView.text = "Correct: "+countCorrect.toString()
            }else{
                countIncorrect++
                incorrectTextView.text ="Inorrect: "+countIncorrect.toString()
            }
            result = randomNum()
            backToActivity()
        }
        val btnAnswer3 = findViewById<Button>(R.id.btnAnswer3)
        btnAnswer3.setOnClickListener {
            val btnAnsId = 3
            val count = check(btnAnsId, result)
            if (count){
                countCorrect++
                correctTextView.text = "Correct: "+countCorrect.toString()
            }else{
                countIncorrect++
                incorrectTextView.text ="Inorrect: "+countIncorrect.toString()
            }
            result = randomNum()
            backToActivity()
        }
    }
    private fun backToActivity() {
        val intent = Intent(sumActivity@this, MainActivity::class.java)
        intent.putExtra("resultCorrect", countCorrect.toString());
        intent.putExtra("resultIncorrect", countIncorrect.toString());
        Thread.sleep(1000)
        startActivity(intent)
    }
    private fun randomNum(): Int {
        val num1 = findViewById<TextView>(R.id.num1)
        val num2 = findViewById<TextView>(R.id.num2)
        val randomnum1 = (0..10).random()
        val randomnum2 = (0..10).random()
        val answer = randomnum1 * randomnum2
        val randomAns1 = (0..100).random()
        val randomAns2 = (0..100).random()
        num1.text = randomnum1.toString()
        num2.text = randomnum2.toString()
        //Toast.makeText(applicationContext, "${randButton}", Toast.LENGTH_LONG).show()
        val id = (0..2).random()
        val buttonName = listOf("btnAnswer1", "btnAnswer2", "btnAnswer3")
        val randButton = buttonName[id]
        var result = 0
        if (id + 1 == 1) {
            btnAnswer1.text = answer.toString()
            btnAnswer2.text = randomAns1.toString()
            btnAnswer3.text = randomAns2.toString()
            result = 1
        } else if (id + 1 == 2) {
            btnAnswer2.text = answer.toString()
            btnAnswer1.text = randomAns1.toString()
            btnAnswer3.text = randomAns2.toString()
            result = 2
        } else if (id + 1 == 3) {
            btnAnswer3.text = answer.toString()
            btnAnswer1.text = randomAns1.toString()
            btnAnswer2.text = randomAns2.toString()
            result = 3
        }
        //Toast.makeText(applicationContext, "${result}", Toast.LENGTH_LONG).show()
        return result

    }


    private fun check(result: Int, btnAnsId: Int): Boolean {
        if (result == btnAnsId) {
            resultTextView.text = "Correct !!"
            return true
        } else if (result != btnAnsId) {
            resultTextView.text = "Inorrect !!"
            return false
        }
        return false
    }
}