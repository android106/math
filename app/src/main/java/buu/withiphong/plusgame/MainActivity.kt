package buu.withiphong.plusgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
var countCorrect = 0
var countIncorrect = 0
class MainActivity : AppCompatActivity() {
    private val REQUEST_MAIN = 11
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val resultCorrect:String = intent.getStringExtra("resultCorrect")?:"0"
        val resultIncorrect:String = intent.getStringExtra("resultIncorrect")?:"0"

        val sumCorrect = findViewById<TextView>(R.id.sumCorrect)
        sumCorrect.setText("Correct: "+resultCorrect)
        val sumIncorrect = findViewById<TextView>(R.id.sumIncorrect)
        sumIncorrect.setText("Incorrect: "+resultIncorrect)


        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener{
            val intent = Intent(MainActivity@this, sumActivity::class.java)
            startActivity(intent)
        }
        val btnDelete = findViewById<Button>(R.id.btnDelete)
        btnDelete.setOnClickListener{
            val intent = Intent(MainActivity@this, deleteActivity::class.java)
            startActivity(intent)
        }
        val btnPower = findViewById<Button>(R.id.btnPower)
        btnPower.setOnClickListener{
            val intent = Intent(MainActivity@this, PowerActivity::class.java)
            startActivity(intent)
        }
    }

}